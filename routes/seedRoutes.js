import express from 'express'
import User from '../models/userModel.js'
import bcrypt from 'bcryptjs'

const router = express.Router()

router.get('/users', async (req, res) => {
  await User.deleteMany({})
  const createdUsers = await User.insertMany([
    {
      username: 'admin',
      password: bcrypt.hashSync('123', 10),
      role: 'Admin',
    },
    {
      username: 'officer',
      password: bcrypt.hashSync('123', 10),
      role: 'Officer',
    },
    {
      username: 'manager',
      password: bcrypt.hashSync('123', 10),
      role: 'Manager',
    },
    {
      username: 'finance',
      password: bcrypt.hashSync('123', 10),
      role: 'Manager',
    },
  ])
  res.send({ createdUsers })
})

export default router
