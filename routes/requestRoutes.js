import express from 'express'
import path from 'path'
import { fileURLToPath } from 'url'
import {
  getRequests,
  createRequest,
  updateRequest,
  updateRequestStatus,
  deleteRequest,
  uploadFile,
} from '../controllers/requestController.js'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const router = express.Router()

router.use('/uploads', express.static(path.join(__dirname, '..', 'uploads')))

router.route('/').get(getRequests).post(createRequest)
router.route('/:id').put(uploadFile, updateRequest).delete(deleteRequest)
router.route('/update/:id').put(uploadFile, updateRequestStatus)

export default router
