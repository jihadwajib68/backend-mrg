import express from 'express'
import {
  deleteUser,
  getAllUsers,
  updateUser,
  login,
  signup,
} from '../controllers/userController.js'

const router = express.Router()

router.route('/').get(getAllUsers)
router.route('/signup').post(signup)
router.route('/login').post(login)
router.route('/:id').put(updateUser).delete(deleteUser)

export default router
