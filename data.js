const data = {
  requests: [
    {
      item: 'Laptop',
      quantity: 10,
      status: 'pending',
    },
    {
      item: 'Mouse',
      quantity: 50,
      status: 'approved',
      transferProofs: '/uploads/transfer.PNG',
    },
    {
      item: 'Keyboard',
      quantity: 30,
      status: 'rejected',
    },
  ],
  users: [
    {
      username: 'officer1',
      password: 'password1',
      role: 'Officer',
    },
    {
      username: 'manager1',
      password: 'password1',
      role: 'Manager',
    },
    {
      username: 'finance1',
      password: 'password1',
      role: 'Finance',
    },
    {
      username: 'admin',
      password: 'admin',
      role: 'Admin',
    },
  ],
}

export default data
