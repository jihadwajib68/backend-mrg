// authMiddleware.js
export const protect = (req, res, next) => {
  const token = req.headers.authorization?.split(' ')[1]
  if (token && token === 'some-hardcoded-token') {
    req.user = { id: 'user-id', role: 'Admin' } // Simulasikan pengguna yang diotentikasi
    next()
  } else {
    res.status(401).json({ message: 'Not authorized, token failed' })
  }
}

export const admin = (req, res, next) => {
  if (req.user && req.user.role === 'Admin') {
    next()
  } else {
    res.status(401).json({ message: 'Not authorized as an admin' })
  }
}
