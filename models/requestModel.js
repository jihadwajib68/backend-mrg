import mongoose from 'mongoose'

const requestSchema = new mongoose.Schema(
  {
    item: { type: String, required: true },
    quantity: { type: Number, required: true },
    status: { type: String, required: true, default: 'pending' },
    rejectionReason: { type: String },
    approvedByManager: { type: Boolean, default: false },
    approvedByFinance: { type: Boolean, default: false },
    transferProof: { type: String }, // Path to the uploaded file
  },
  {
    timestamps: true,
  }
)

const Request = mongoose.model('Request', requestSchema)

export default Request
