import Request from '../models/requestModel.js'
import multer from 'multer'
import { CloudinaryStorage } from 'multer-storage-cloudinary'
import { v2 as cloudinary } from 'cloudinary'

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
})

const storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: {
    folder: 'uploads',
    format: async (req, file) => 'png' || 'jpg', // set format sesuai dengan kebutuhan Anda
    public_id: (req, file) => `${Date.now()}-${file.originalname}`,
  },
})

const upload = multer({ storage })
export const uploadFile = upload.single('transferProof')

export const getRequests = async (req, res) => {
  try {
    const requests = await Request.find({})
    res.json(requests)
  } catch (error) {
    res.status(500).send({ message: error.message })
  }
}

export const createRequest = async (req, res) => {
  const newRequest = new Request({
    item: req.body.item,
    quantity: req.body.quantity,
    status: req.body.status || 'pending',
  })

  try {
    const createdRequest = await newRequest.save()
    res.status(201).json(createdRequest)
  } catch (error) {
    res.status(500).send({ message: error.message })
  }
}

export const updateRequestStatus = async (req, res) => {
  try {
    console.log('Update Request Status:', req.body)
    console.log('File:', req.file)

    const request = await Request.findById(req.params.id)
    if (request) {
      if (request.status !== 'pending') {
        return res
          .status(400)
          .send({ message: 'Request has already been processed.' })
      }

      if (
        req.body.approvedByManager !== undefined &&
        !request.approvedByFinance
      ) {
        request.approvedByManager = req.body.approvedByManager
      }

      if (
        req.body.approvedByFinance !== undefined &&
        !request.approvedByManager
      ) {
        request.approvedByFinance = req.body.approvedByFinance
      }

      request.status = req.body.status || request.status
      request.rejectionReason =
        req.body.rejectionReason || request.rejectionReason

      if (req.file) {
        request.transferProof = req.file.path // Save the file path to the database
      }

      const updatedRequest = await request.save()
      res.json(updatedRequest)
    } else {
      res.status(404).send({ message: 'Request Not Found' })
    }
  } catch (error) {
    console.error('Error updating request status:', error)
    res.status(500).send({ message: error.message })
  }
}

export const updateRequest = async (req, res) => {
  try {
    console.log('Update Request:', req.body)
    console.log('File:', req.file)

    const request = await Request.findById(req.params.id)
    if (request) {
      if (req.file) {
        request.transferProof = req.file.path // Save the file path to the database
      }

      request.item = req.body.item || request.item
      request.quantity = req.body.quantity || request.quantity
      request.status = req.body.status || request.status
      request.rejectionReason =
        req.body.rejectionReason || request.rejectionReason

      const updatedRequest = await request.save()
      res.json(updatedRequest)
    } else {
      res.status(404).send({ message: 'Request Not Found' })
    }
  } catch (error) {
    console.error('Error updating request:', error)
    res.status(500).send({ message: error.message })
  }
}

export const deleteRequest = async (req, res) => {
  const { id } = req.params

  try {
    const request = await Request.findById(id)
    if (!request) {
      return res.status(404).json({ message: 'Request not found' })
    }

    await request.deleteOne()
    res.json({ message: 'Request deleted successfully' })
  } catch (error) {
    console.error('Error deleting request:', error)
    res.status(500).json({ message: error.message })
  }
}
