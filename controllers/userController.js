import User from '../models/userModel.js'

// Handle user signup
export const signup = async (req, res) => {
  const { username, password, role } = req.body

  if (!username || !password || !role) {
    return res.status(400).send({ message: 'All fields are required' })
  }

  const userExists = await User.findOne({ username })

  if (userExists) {
    return res.status(400).send({ message: 'User already exists' })
  }

  const user = new User({ username, password, role })

  try {
    const createdUser = await user.save()
    res.status(201).send(createdUser)
  } catch (error) {
    res.status(500).send({ message: error.message })
  }
}

// Handle user login
export const login = async (req, res) => {
  const { username, password } = req.body

  try {
    const user = await User.findOne({ username })

    if (user && (await user.matchPassword(password))) {
      res.status(200).send({
        _id: user._id,
        username: user.username,
        role: user.role,
      })
    } else {
      res.status(400).send({ message: 'Invalid username or password' })
    }
  } catch (error) {
    res.status(500).send({ message: error.message })
  }
}

export const getAllUsers = async (req, res) => {
  try {
    const users = await User.find({})
    res.json(users)
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

export const updateUser = async (req, res) => {
  const { id } = req.params
  const { username, role } = req.body

  try {
    const user = await User.findById(id)
    if (!user) {
      return res.status(404).json({ message: 'User not found' })
    }

    user.username = username || user.username
    user.role = role || user.role

    await user.save()
    res.json({ message: 'User updated successfully', user })
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

export const deleteUser = async (req, res) => {
  const { id } = req.params

  try {
    const user = await User.findById(id)
    if (!user) {
      return res.status(404).json({ message: 'User not found' })
    }

    await user.deleteOne() // Ganti remove dengan deleteOne
    res.json({ message: 'User deleted successfully' })
  } catch (error) {
    console.error('Error deleting user:', error)
    res.status(500).json({ message: error.message })
  }
}
