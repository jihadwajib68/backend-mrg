import Request from '../models/requestModel.js'
import User from '../models/userModel.js'
import data from '../data.js'

export const seedRequests = async (req, res) => {
  await Request.deleteMany({})
  const createdRequests = await Request.insertMany(data.requests)
  res.send({ createdRequests })
}

export const seedUsers = async (req, res) => {
  await User.deleteMany({})
  const createdUsers = await User.insertMany(data.users)
  res.send({ createdUsers })
}
